import numpy as np

from scripts.utils import load
from scripts.bigger_picture import bigger_picture
from scripts.machine_learning import machine_learning
from scripts.reference_data import reference
from scripts.result import result
from scripts.polyfit import fit

if __name__ == '__main__':
    # # Step one
    # print('Running first step \'big picture\'')
    # print('-' * 80)
    # bigger_picture()
    # print()
    #
    # # Step two
    # print('Running second step \'big picture with reference\'')
    # print('-' * 80)
    # reference()
    # print()

    x = np.array([x_ for x_ in np.arange(-10, 10, .1) if not (-2 < x_ < -1 or 1 < x_ < 2)])
    y = np.array([load(x_) for x_ in x])

    # Step three
    print('Running third step \'machine learning\'')
    print('-' * 80)
    machine_learning(x, y)
    print()

    print('Running numpy.polyfit()')
    print('-' * 80)
    fit(x, y)
    print()

    # # Step three
    # print('Drawing result function...')
    # result()

