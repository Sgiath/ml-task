from typing import Iterable

import numpy as np
import plotly
import plotly.graph_objs as go

from .utils import load

__all__ = ['bigger_picture', ]


def draw_graph(x_data: Iterable, i: int) -> None:
    plotly.offline.plot({
        'data': [go.Scatter(x=x_data, y=[load(x) for x in x_data], mode='lines'), ],
        'layout': go.Layout(title='01 Bigger picture'),
    }, filename='data/01-{}_bigger_picture.html'.format(i))


def bigger_picture() -> None:
    print('Drawing first graph...')
    draw_graph(np.arange(-1000, 1000, 10), 1)
    print('Drawing second graph...')
    draw_graph(np.arange(-100, 100, 1), 2)
    print('Drawing third graph...')
    draw_graph(np.arange(-5, 5, .1), 3)
