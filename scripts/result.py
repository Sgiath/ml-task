import math

import numpy as np
import plotly
import plotly.graph_objs as go

from .utils import load


def result() -> None:
    x_full = np.arange(-5, 5, .01)
    y_full = [math.pow(x, 4) - 5 * math.pow(x, 2) + 5 * x - 6 for x in x_full]

    x_data = [x for x in x_full if not (-2 < x < -1 or 1 < x < 2)]
    y_data = [load(x) for x in x_data]

    plotly.offline.plot({
        'data': [
            go.Scatter(x=x_data, y=y_data, mode='lines', name='data'),
            go.Scatter(x=x_full, y=y_full, mode='lines', name='result function'),
        ],
        'layout': go.Layout(title='04 Result'),
    }, filename='data/04_result.html')
