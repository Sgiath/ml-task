from typing import Iterable

import math

import numpy as np
import requests

__all__ = ['load', 'load_mean', ]


def load(x: float) -> float:
    """Load data from endpoint

    Arguments:
        x: x value to load

    Returns:
        y value from endpoint, 0 if there are no data
    """
    # Speed up hack :)
    if -2 < x < -1 or 1 < x < 2:
        return float(0)

    r = requests.get('http://165.227.157.145:8080/api/do_measurement?x={}'.format(x))
    return float(r.json()['data']['y'])


def load_mean(x: float, samples: int) -> float:
    """Load multiple y for x and return their mean

    Arguments:
        x: x value to load
        samples: number of samples to load

    Return:
        mean of the y values
    """
    return float(np.mean([load(x) for _ in range(samples)]))


def compute(x: Iterable, a: float = 1.0, b: float = 0.0, c: float = -5.0, d: float = 5.0, e: float = -6.0) -> Iterable:
    return [a * math.pow(x, 4) + b * math.pow(x, 3) + c * math.pow(x, 2) + d * x + e for x in x]
