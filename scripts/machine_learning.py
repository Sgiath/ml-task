from typing import Iterable

import numpy as np
import plotly
import plotly.graph_objs as go
from sklearn.metrics import mean_squared_error
import tensorflow as tf

from .utils import load_mean, compute


def model_fn(features, labels, mode):
    """Model function for Estimator"""
    a = tf.get_variable('a', [1], dtype=tf.float64)
    b = tf.get_variable('b', [1], dtype=tf.float64)
    c = tf.get_variable('c', [1], dtype=tf.float64)
    d = tf.get_variable('d', [1], dtype=tf.float64)
    e = tf.get_variable('e', [1], dtype=tf.float64)

    y = a * tf.pow(features['x'], 4) + b * tf.pow(features['x'], 3) + c * tf.pow(features['x'], 2) + d * features['x'] + e
    loss = tf.reduce_sum(tf.square(y - labels))

    global_step = tf.train.get_global_step()
    optimizer = tf.train.AdamOptimizer()
    train = tf.group(optimizer.minimize(loss), tf.assign_add(global_step, 1))

    return tf.estimator.EstimatorSpec(mode=mode, predictions=y, loss=loss, train_op=train)


def ml_estimator():
    """ML with using Tensoflow's Estimators

    I am not using it for the result because I don't know how to get the parameters back from the model but this is
    preferred method how to use Tensorflow
    """
    estimator = tf.estimator.Estimator(model_fn=model_fn)

    x_full = np.arange(-5, 5, .1)
    x_train = np.array([x_ for x_ in x_full if not (-2 < x_ < -1 or 1 < x_ < 2)])
    y_train = np.array([load_mean(x_, 2) for x_ in x_train])

    input_fn = tf.estimator.inputs.numpy_input_fn({'x': x_train}, y_train, shuffle=False)

    estimator.train(input_fn=input_fn, steps=10000)


def variable_summaries(var):
    """Attach a lot of summaries to a Tensor (for TensorBoard visualization)."""
    with tf.name_scope('summaries'):
        mean = tf.reduce_mean(var)
        tf.summary.scalar('mean', mean)
        with tf.name_scope('stddev'):
            stddev = tf.sqrt(tf.reduce_mean(tf.square(var - mean)))
        tf.summary.scalar('stddev', stddev)
        tf.summary.scalar('max', tf.reduce_max(var))
        tf.summary.scalar('min', tf.reduce_min(var))
        tf.summary.histogram('histogram', var)


def machine_learning(x_train: Iterable, y_train: Iterable):
    # Function parameters
    with tf.name_scope('equation_params'):
        with tf.name_scope('a_param'):
            a = tf.get_variable('a', shape=[1], dtype=tf.float32, initializer=tf.contrib.layers.xavier_initializer())
            variable_summaries(a)
        with tf.name_scope('b_param'):
            b = tf.get_variable('b', shape=[1], dtype=tf.float32, initializer=tf.contrib.layers.xavier_initializer())
            variable_summaries(b)
        with tf.name_scope('c_param'):
            c = tf.get_variable('c', shape=[1], dtype=tf.float32, initializer=tf.contrib.layers.xavier_initializer())
            variable_summaries(c)
        with tf.name_scope('d_param'):
            d = tf.get_variable('d', shape=[1], dtype=tf.float32, initializer=tf.contrib.layers.xavier_initializer())
            variable_summaries(d)
        with tf.name_scope('e_param'):
            e = tf.get_variable('e', shape=[1], dtype=tf.float32, initializer=tf.contrib.layers.xavier_initializer())
            variable_summaries(e)

    # Function variable
    x = tf.placeholder(tf.float32, name='x')
    # Function result
    y = tf.placeholder(tf.float32, name='y')

    # Model of the function
    model = (
        tf.multiply(a, tf.pow(x, 4, name='x4'), name='ax4') +
        tf.multiply(b, tf.pow(x, 3, name='x3'), name='bx3') +
        tf.multiply(c, tf.pow(x, 2, name='x2'), name='cx2') +
        tf.multiply(d, x, name='dx') + e
    )

    # Calculated loss
    with tf.name_scope('loss'):
        loss = tf.reduce_sum(tf.square(model - y), name='loss')
        variable_summaries(loss)

    # Optimizer should minimize loss
    optimizer = tf.train.AdamOptimizer(name='optimizer')
    train = optimizer.minimize(loss)

    with tf.Session() as sess:
        # Logs for TensorBoard
        merged = tf.summary.merge_all()
        file_writer = tf.summary.FileWriter('ml-graph', sess.graph)

        # Initialize the environment
        init = tf.global_variables_initializer()
        sess.run(init)

        # Print initial data with calculated loss for check
        print('Initial data')
        params = sess.run([a, b, c, d, e], {x: x_train, y: y_train})
        print('a: {}, b: {}, c: {}, d: {}, e: {}, loss: {}'.format(
                *params, mean_squared_error(y_train, compute(x_train, *params))))
        print()

        # Run the training cycles
        for i in range(50000):
            # Train
            sess.run(train, {x: x_train, y: y_train})

            if i % 1000 == 0:
                # Get summary for TensorBoard
                run_options = tf.RunOptions(trace_level=tf.RunOptions.FULL_TRACE)
                run_metadata = tf.RunMetadata()
                summary = sess.run(merged, {x: x_train, y: y_train}, options=run_options, run_metadata=run_metadata)
                file_writer.add_summary(summary, i)
                file_writer.add_run_metadata(run_metadata, 'step{}'.format(i))

        # Load current (trained) data
        params = sess.run([a, b, c, d, e], {x: x_train, y: y_train})

        # Print result
        print('Learned data')
        print('a: {}, b: {}, c: {}, d: {}, e: {}, loss: {}'.format(*params, mean_squared_error(
                        y_train, compute(x_train, float(params[0][0]), float(params[1][0]), float(params[2][0]),
                                         float(params[3][0]), float(params[4][0])))))

        # Calculate data for the discovered function
        y_data = compute(x_train, float(params[0][0]), float(params[1][0]), float(params[2][0]), float(params[3][0]),
                         float(params[4][0]))

        # Print both functions in one graph
        plotly.offline.plot({
            'data': [
                go.Scatter(x=x_train, y=y_train, mode='lines', name='data'),
                go.Scatter(x=x_train, y=y_data, mode='lines', name='trained'),
            ],
            'layout': go.Layout(title='03 Trained data'),
        }, filename='data/03_trained_data.html')
