import numpy as np
import plotly
import plotly.graph_objs as go

from .utils import load

__all__ = ['reference', ]


def reference() -> None:
    x_full = np.arange(-10, 10, .1)
    x_data = [x for x in x_full if not (-2 < x < -1 or 1 < x < 2)]

    print('Drawing graph with reference...')
    plotly.offline.plot({
        'data': [
            go.Scatter(x=x_data, y=[load(x) for x in x_data], mode='lines', name='data'),
            go.Scatter(x=x_full, y=np.power(x_full, 2), mode='lines', name='pow2'),
            go.Scatter(x=x_full, y=np.power(x_full, 4), mode='lines', name='pow4'),
        ],
        'layout': go.Layout(title='02 Bigger picture with reference'),
    }, filename='data/02_reference.html')
