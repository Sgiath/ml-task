from typing import Iterable

import numpy as np
from sklearn.metrics import mean_squared_error

from .utils import compute


def fit(x: Iterable, y: Iterable):
    params = np.polyfit(x, y, 4)
    print('a: {}, b: {}, c: {}, d: {}, e: {}, loss: {}'.format(*params, mean_squared_error(y, compute(x, *params))))
