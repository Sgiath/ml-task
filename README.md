# Entry task for Machine Learning weekend

**Author:** Filip Vavera <FilipVavera@sgiath.net>

## How I proceeded
 1. Get a bigger picture
    * Load a lots of points and draw them in graph
    * Code can be found in `scripts/bigger_picture.py` and generated graph in `data/01-[1-3]_bigger_picture.html`
    * **Result:** function seems to be polynomial with even *order*
    * *Note:* in the closer examination it seems that endpoint returns `null` for `-2 < x < -1` and `1 < x < 2` so in every other scripts I will be omitting this values
 2. Compare it with reference
    * Again load lots of data and draw it in one graph with reference functions:
      * *f(x) = x^2*
      * *f(x) = x^4*
    * Code can be found in `scripts/reference_data.py` and generated graph in `data/02_reference.html`
    * **Result:** data from the server almost exactly copy the function *f(x) = x^4* so we can assume that the function we are looking for is polynomial function 4th order:
      * *f(x) = ax^4 + bx^3 + cx^2 + dx + e*
 3. Implement ML algorithm which will try to find correct parameters (a - e)
    * Using Tensorflow for this task
    * Code can be found in `scripts/machine_learning.py` and generated graph in `data/03_trained_data.html`
    * *Note:* be patient script is loading a lots data (8000 requests on the endpoint)
    * **Result:** by running ML algorithm multiple times it seems that most probable values for the equation parameters will be [1, 0, -5, 5, -6]. In other words this is the function we are looking for:
      * *f(x) = x^4 - 5x^2 + 5x - 6*

## How to run my solution
 * Install requirements: `pip install -r requirements.txt`
 * Run the `main.py` script: `python main.py` (the best way is to run it through PyCharm because of problems with PYTHON_PATH :) )
 * Or you can view the graphs without regenerating them in the browser (in `data/` folder)
 * You can also view TensorBoard for my ML model by running `tensorboard --logdir ml-graph` and then opening `localhost:6006` in browser
